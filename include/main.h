/******************************************************************************
* File:             main.h
*
* Author:           Benjamin James  
* Created:          05/21/21 
* Description:      Main program header file which contains pin defenitions
*                   and important function declarations.
*****************************************************************************/

#ifndef __MAIN_H
#define __MAIN_H

#include <stm32f1xx_hal.h>

TIM_HandleTypeDef htim4;

void Error_handler(void);
void SysClk_Config(void);

#define PWM_GPIO_Port GPIOB
#define PWM_Pin GPIO_PIN_9

#endif
