/******************************************************************************
* File:             tim.h
*
* Author:           Benjamin James  
* Created:          05/22/21 
* Description:      This is the header file for the PWM initialization
*****************************************************************************/

#ifndef __TIM_H__
#define __TIM_H__

#include "stm32f1xx_hal.h"
#include "main.h"

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
void TIM4_Init(void);

#endif /* ifndef __TIM_H__ */
