# STM32F103C8 Example

## Variable dutycycle PWM signal generation

This is a example code to generate a variable PWM signal.
The timer 4 channel 4 is used here to generate the PWM output (PIN PB9 in bluepill)

The code is written using platformio and choosing the stm32cube framework,
which contains the basic Hardware Abstraction Layer (HAL) drivers for STM32 development.

### How to run this example
---------------------------

##### Step 1: Clone the repository using git.

##### Step 2: Connect the stlink to your STM32F103C8 chip / bluepill.

##### Step 3: Put your BOOT0 jumper to 1 position.

##### Step 4: Reset your chip by pressing the reset button in the board.

##### Step 5: Open terminal and run the following command ```platformio run --target upload```

##### Step 6: After uploading, place the BOOT0 jumper back to the 0 position.

##### Step 7: Connet a LED to the PB9 pin of the microcontroller.

##### Step 8: Reset the chip again using the reset button.
